import React from 'react'
import CardItem from './CardItem'
import "./Cards.css";

function Cards() {
    return (
        <div className="cards">
            <h1>OUR TEAM</h1>
            <div className="cards__container">
                <div className="cards__wrappper">
                    <ul className="cards__items">
                        <CardItem 
                            src="images/img-9.jpg"
                            text="Muhirwa Clement"
                            label="MD"
                            path="/services"
                        />
                        <CardItem 
                            src="images/img-9.jpg"
                            text="Dukuzeyezu Placide"
                            label="CTO"
                            path="/services"
                        />
                        <CardItem 
                            src="images/img-9.jpg"
                            text="Salvin"
                            label="FULLSTACK"
                            path="/services"
                        />
                    </ul>
                    <ul className="cards__items">
                        <CardItem 
                            src="images/img-9.jpg"
                            text="we use have and  blabla for the back end."
                            label="BACK-END"
                            path="/services"
                        />
                        <CardItem 
                            src="images/img-9.jpg"
                            text="we use react for the front end."
                            label="FRONT-END"
                            path="/services"
                        />
                        <CardItem 
                            src="images/img-9.jpg"
                            text="We use figma in china we use lanhuapp"
                            label="DESIGNER"
                            path="/services"
                        />
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Cards
